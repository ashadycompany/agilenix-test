const jwt = require('../helpers/JWT');
const User = require('../models/user').User;
const ping = require('ping');
const secret = process.env.JWT_SECRET;
const toPing = 'google.com';



module.exports.info = async (req, res, next) => {
    const { auth } = req.query;
    let data, user;

    try{

        data = await jwt.verify(auth, secret);
        user = await User.findOne({id: data.userId});

    } catch (e) { throw e; }

    res.json({
        message: "User info",
        id: user.id,
        idType: `${user.idType === 0 ? 'Email' : 'Phone'}`
    });
}

module.exports.latency = async (req, res, next) => {

    let start = process.hrtime();

    try{
        await ping.promise.probe(toPing);
    } catch (e) { throw e; }

    let finish = process.hrtime(start);

    res.json({
        message:"Latency reaching google.com",
        latency:`It took is ${finish[0]} seconds and ${finish[1]} nanoseconds`
    });

}

module.exports.logout = async (req, res, next) => {
    const { auth, all } = req.query;

    try{
        const data = await jwt.verify(auth, secret);
        await User.findOneAndUpdate({id:data.userId}, {logoutTime: new Date()});
    } catch (e) { throw e; }

    res.json({
        message:"Logged out successfully"
    });
}