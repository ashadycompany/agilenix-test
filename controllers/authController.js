const jwt = require('../helpers/JWT');
const bcrypt = require('bcrypt');
const User = require('../models/user').User;
const check = require('../helpers/checkData');
const secret = process.env.JWT_SECRET;
const refreshSecret = process.env.REFRESH_SECRET;



module.exports.signup = async (req, res, next) => {
    if(req.body.id && req.body.password){
        const { id, password } = req.body;
        let idType;

        if(check.checkMail(id)) idType = 0;
        else if(check.checkPhone(id)) idType = 1;
        else return res.end("Wrong login");

        if(!check.checkPassword(password)) return res.end("Wrong password");
        
        try{
            const userCheck = await User.findOne({id:id});
            if(userCheck) return res.end("Wrong request body");
            const hash = await bcrypt.hash(password, 12);
            await User.create({id: id, hash: hash, idType: idType});

        } catch(e) { throw e; }

        res.end("User created successfully");
    }
    else{
        res.end("Wrong request body");
    }
}

module.exports.signin = async (req, res, next) => {
    if(req.body.id && req.body.password){
        const { id, password } = req.body;
        try{
            const user = await User.findOne({id:id});

            if(user){

                const passwordCheck = await bcrypt.compare(password, user.hash);

                if(passwordCheck){
                    const expiryDate = Math.floor(new Date() / 1000) + (60 * 10);
                    const token = await jwt.sign({  userId:id,
                                                    exp: expiryDate},
                                                    secret);
                    const refreshToken = await jwt.sign({   userId: id,
                                                            exp: expiryDate * 5}, 
                                                            refreshSecret);

                    await User.findOneAndUpdate({_id: user._id}, {refreshToken: refreshToken});

                    res.json({
                        message: "Here is your token",
                        token: token,
                        refreshToken: refreshToken
                    });
                }

            }
            else {
                res.end("Wrong credentials");
            }
        } catch(e) { throw e; }
    }
    else{
        res.end("Wrong request body");
    }
}