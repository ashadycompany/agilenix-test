const mongoose = require('mongoose');
const connection = process.env.MONGO_CONNECTION_STRING;

mongoose.connect(connection, { useNewUrlParser: true }, (err) => {
    if (err) throw err;
    else console.log("connected");
});

module.exports = mongoose;