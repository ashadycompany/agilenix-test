# AgileNix Test

#What is this?

A test project.

#Requirements

NodeJS https://nodejs.org/ MongoDB https://docs.mongodb.com/manual/installation/#tutorials

#Configuration

Connection string is taken from process.env.MONGO_CONNECTION_STRING. Define one in your environment or feel free to replace it in ./db/index
Secret and refresh secret are taken from process.env.JWT_SECRET and process.env.REFRESH_SECRET respectively.

#How to run

Run '>npm start' in project folder to start the server. You can also use the '> node ./bin/www' command from the main folder.
