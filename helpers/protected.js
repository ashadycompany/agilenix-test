const jwt = require('./JWT');
const User = require('../models/user').User;
const secret = process.env.JWT_SECRET;
const refreshSecret = process.env.REFRESH_SECRET;

module.exports.checkAuth = async (req, res, next) => {
    let token = req.body.auth ? req.body.auth : req.query.auth;

    if(!token) return res.end("Token is missing");

    try{
        const data = await jwt.verify(token, secret);
        if(data){

            const user = await User.findOne({id:data.userId});

            if(user){

                if(user.logoutTime && Date.parse(user.logoutTime) > data.iat * 1000) return res.end("Not allowed");
                else return next();

                }

            }
    } catch (e) {
        
        if (e.name === 'TokenExpiredError'){
            if(req.body.refreshToken && req.query.refreshToken){

                let refresh = req.body.refreshToken ? req.body.refreshToken : req.query.refreshToken;
                const refreshData = await jwt.verify(refresh, refreshSecret);

                if(refreshData){
                    const user = await User.findOne({id:refreshData.userId});
                    
                    if(user){
                        const token = jwt.sign({    userId:id,
                                                    exp: expiryDate},
                                                    secret);

                        res.json({
                            message:"Your old token expired, but your refresh token was valid, here is a new token",
                            token:token
                        })
                    }
                }

            }
            else return res.end("token expired");
        }
        else throw e;
    }

}