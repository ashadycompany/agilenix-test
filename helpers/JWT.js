const jwt = require('jsonwebtoken');


module.exports = {
    sign: (payload, secret, options = null) => {
        return new Promise(function(resolve, reject) {
            jwt.sign(payload, secret, options, function(err, token){
                if(err) reject(err);
                else resolve(token);
            })
        })
    },
    
    verify: (token, secret, options = null) => {
        return new Promise(function(resolve, reject) {
            jwt.verify(token, secret, options, function(err, decoded){
                if(err) reject(err);
                else resolve(decoded);
            })
        })
    }
}