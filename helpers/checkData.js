


module.exports.checkMail = (mail) => {
    if (mail.search(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/ig) === -1 || mail.length>256) return false;
    return true;
}
module.exports.checkPassword = (password) => {
    if(password.search(/^[a-zA-Z0-9!@#$&()`.+,/"-]*$/ig) === -1 || password.length < 8 || password.length > 64) return false;
    return true;
}
module.exports.checkPhone = (phone) => {
    if(phone.search(/^\+\d{12}$/) === -1) return false;
    else return true;
}