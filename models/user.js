const mongoose = require('../db');

const schema = mongoose.Schema({
    id:{
        type:String,
        required:true
    },
    hash:{
        type:String,
        required:true
    },
    idType:{
        type:Boolean,
        required:true
    },
    refreshToken:{
        type:String
    },
    logoutTime:{
        type:String
    }
});

const User = mongoose.model('User', schema);

module.exports.User = User;