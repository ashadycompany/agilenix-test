const express = require('express');
const router = express.Router();

router.post('/signup', require('../controllers/authController').signup);
router.post('/signin', require('../controllers/authController').signin);

router.get('/info', require('../helpers/protected').checkAuth, require('../controllers/mainController').info);
router.get('/latency', require('../helpers/protected').checkAuth, require('../controllers/mainController').latency);
router.get('/logout', require('../helpers/protected').checkAuth, require('../controllers/mainController').logout);

module.exports = router;
